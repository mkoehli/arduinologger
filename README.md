# Arduino Nuclear Environmnetal Logger

Data logger based on the Arduino DUE with SD card, OLED display, real time clock, SDI-12 and I2C interface, GSM and LoRa telemetry implementation. 

## Getting started

Board:
Use KiCAD https://www.kicad.org/

Firmware:
Arduino Studio https://www.arduino.cc/en/software

## Contributors

Markus Köhli, Physikalisches Institut, Heidelberg University  
Jannis Weimar, Physikalisches Institut, Heidelberg University  
Ulrich Schmidt, Physikalisches Institut, Heidelberg University


## Acknowledgment
We acknowledge the electronics workshop of the Physiklaisches Institut, Heidelberg University, for their steady support and helpful suggestions. We acknowledge StyX Neutronica for constant support of this project. 

## License
Board files and related documents are licence under CC BY-SA 4.0. Firmware code is licenced under GNU GPLv3.

## Project status
Readme Files to be added.
